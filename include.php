<?
	use Bitrix\Main\Loader;
	use Bitrix\Main\Localization\Loc;

	Loader::includeModule('altit.pagespeed');

	Loc::loadMessages(__FILE__);

	Loader::registerAutoloadClasses('altit.pagespeed', [
		'WebPConvert\WebPConvert' => 'lib/webp-convert/webp-convert.inc',
		'Altit\PageSpeed\PageSpeedTable' => 'lib/PageSpeedTable.php',
        'Altit\PageSpeed\Main' => 'lib/Main.php',
        'Altit\PageSpeed\Helper' => 'lib/Helper.php',
		'Altit\PageSpeed\CompressHTML' => 'lib/CompressHTML.php',
	]);