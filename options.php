<?Php

use Bitrix\Main\Localization\Loc;
use	Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Altit\PageSpeed\Main; 
use Altit\PageSpeed\Helper;

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialcharsbx($request['mid'] != '' ? $request['mid'] : $request['id']);


Loader::includeModule($module_id);

$fontCheck = (Option::get($module_id, 'active_inline_css_font', 'N') === 'Y') ? true : false;
Main::fontCache($fontCheck);

if (empty(Option::get($module_id, 'document_root', '')))
{
	Option::set($module_id, 'document_root', $_SERVER['DOCUMENT_ROOT']);
}

$convertCheck = Main::convertWebpTest();

if($convertCheck['success']) 
{
	$lib = call_user_func_array('array_merge', array_filter($convertCheck['converters'], function($v) {	
		if ($v['success'])
			return $v; 
	}));
	$gdVer = (int)mb_substr(preg_replace("/[^0-9]/", '', gd_info()['GD Version']), 0, -1);

	$text = 'Тестовая конвертация изображения прошла успешно.';
	$textDetail = 'Использованная библиотека: ' . $lib['name'];

	if ($lib['name'] === 'gd' && $gdVer < 22)
		$textDetail .= '<br/>Вы используете устаревшую версию библиотеку GD.<br/>Возможно некорректная обработка прозрачных изображений (.png).<br/>Выключите конвертирование “png” в настройках при возникновении ошибок.';

	$message = new CAdminMessage([
		'MESSAGE' => $text,
		'TYPE' => 'OK',
		'DETAILS' => $textDetail,
		'HTML' => true
	]);
} else {
	$text = 'Не удалось конвертировать изображение.';
	$textDetail = 'Установите пожалуйста на сервер одну из библиотек: Imagick, Gmagick, Vips, GD.<br/>Вы можете обратиться за помощью  к тех. поддержке хостинга.';
	$message = new CAdminMessage([
		'MESSAGE' => $text,
		'TYPE' => 'ERROR',
		'DETAILS' => $textDetail,
		'HTML' => true
	]);

	Option::set($module_id, 'active_convert_webp', 'N');
}

use Altit\PageSpeed\PageSpeedTable;

// echo Altit\PageSpeed\PageSpeedTable::getEntity()->createDbTable();


// $db = Bitrix\Main\Application::getConnection();
// $db->dropTable(PageSpeedTable::getTableName());


// $addedResult = PageSpeedTable::add([
//     'TIME' => filectime($_SERVER['DOCUMENT_ROOT'].'/local/templates/dentaline/img/logo.png'),
// 	'SRC' => '/local/templates/dentaline/img/logo.png',
// ]);




// $test = PageSpeedTable::getList([
//     'order' => [
// 		'ID' => 'ASC'
// 	],
//     'limit' => 10
// ])->fetchAll();

// pr($test );

$aTabs = array(
	array(
		'DIV' 	  => 'edit',
		'TAB' 	  => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME'),
		'TITLE'   => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME'),
		'OPTIONS' => array(
			array(
				'active_module',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_SWITCH_ON'),
				'N',
				array('checkbox')
			),
			array(
				'jquery',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_JQUERY_ON'),
				'N',
				array('checkbox')
			),
		),
	),
	array(
		'DIV' 	  => 'edit2',
		'TAB' 	  => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME2'),
		'TITLE'   => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME2'),
		'OPTIONS' => array(		
			Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LAZY_LOAD'),
			array(
				'active_lazy_load_img',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LAZY_LOAD_ON'),
				'N',
				array('checkbox')
			),
			array(
				'active_lazy_load_img_class',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LAZY_LOAD_CLASS'),
				'lazy_loading',
				array('text', 50)
			),
			array(
				'active_lazy_load_js',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LAZY_LOAD_JS'),
				'Y',
				array('checkbox')
			),
			array(
				'active_lazy_load_img_preloader_path',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LAZY_LOAD_PR_PATH'),
				'/bitrix/images/' . $module_id . '/loading.gif',
				array('text', 50)
			),
			array(
				'active_lazy_load_img_preloader_color',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LAZY_LOAD_PR_COLOR'),
				'0',
				array('text', 50)
			),
			Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LOAD_SCRIPT'),
			array(
				'scripts',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LOAD_SCRIPT_ON'),
				'N',
				array('checkbox')
			),
			array(
				'scripts_value',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_LOAD_SCRIPT_VALUE'),
				'',
				array('textarea', 15, 70),
			),	
			Loc::getMessage('ALTIT_PAGE_SPEED_TAB_IFRAME'),
			array(
				'iframe_on',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_IFRAME_ON'),
				'N',
				array('checkbox')
			),		
		),
	),
	array(
		'DIV' 	  => 'edit3',
		'TAB' 	  => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME3'),
		'TITLE'   => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME3'),
		'OPTIONS' => array(		
			array(
				'inline_css',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_INLINE_CSS'),
				'N',
				array('checkbox')
			),
			Loc::getMessage('ALTIT_PAGE_SPEED_TAB_DOP_SETTING'),
			array(
				'active_inline_css_compress',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_INLINE_CSS_COMPRESS'),
				'N',
				array('checkbox')
			),			
			array(
				'active_inline_css_external',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_INLINE_CSS_EXTERNAL'),
				'N',
				array('checkbox')
			),
			array(
				'active_inline_css_font',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_INLINE_CSS_FONT'),
				'N',
				array('checkbox')
			),
			Loc::getMessage('ALTIT_PAGE_SPEED_TAB_INLINE_CSS_FILE'),
			array(
				'active_inline_css_exceptions',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_INLINE_CSS_EXCEPTIONS_ON'),
				'N',
				array('checkbox')
			),
			array(
				'inline_css_exceptions_val',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_INLINE_CSS_EXCEPTIONS_VAL'),
				'',
				array('textarea', 10, 50)
			),
		),
	),
	array(
		'DIV' 	  => 'edit4',
		'TAB' 	  => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME4'),
		'TITLE'   => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME4'),
		'OPTIONS' => array(		
			array(
				'active_convert_webp',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_CONVERT_WEBP'),
				'N',
				array('checkbox')
			),	
			array(
				'active_convert_webp_png',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_CONVERT_WEBP_PNG'),
				'Y',
				array('checkbox')
			),
		),
	),
	array(
		'DIV' 	  => 'edit5',
		'TAB' 	  => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME5'),
		'TITLE'   => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_NAME5'),
		'OPTIONS' => array(		
			Loc::getMessage('ALTIT_PAGE_SPEED_TAB_COMPRESS_HTML'),
			array(
				'active_compress_html',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_COMPRESS_HTML_ON'),
				'N',
				array('checkbox')
			),
			array(
				'active_compress_html_css',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_COMPRESS_HTML_CSS'),
				'Y',
				array('checkbox')
			),
			array(
				'active_compress_html_js',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_COMPRESS_HTML_JS'),
				'N',
				array('checkbox')
			),
			array(
				'active_compress_html_comment',
				Loc::getMessage('ALTIT_PAGE_SPEED_TAB_COMPRESS_HTML_COMMENT'),
				'N',
				array('checkbox')
			),		
		),
	),
);

$tabControl = new CAdminTabControl(
	'tabControl',
	$aTabs
);

if($request->isPost() && check_bitrix_sessid())
{
	foreach($aTabs as $tab)
	{
		foreach($tab['OPTIONS'] as $option)
		{
			if(!is_array($option) || $option['note'])
				continue;
			
			if($request['apply'])
			{
				CAdminNotify::Add(
					[
						'MESSAGE' => Loc::getMessage('ALTIT_PAGE_SPEED_TAB_INLINE_CSS_CACHE'),  
						'TAG' => $module_id . '_clear_cache', 
						'MODULE_ID' => $module_id, 
						'ENABLE_CLOSE' => 'Y'
					]
				);

				$optionValue = $request->getPost($option[0]);

				if($option[0] == 'active_module'){

					if($optionValue == ''){

						$optionValue = 'N';
					}
				}

				Option::set($module_id, $option[0], $optionValue);

			} elseif($request['default']) {

				Option::set($module_id, $option[0], $option[2]);
			}
		}
	}
	unset($tab, $option);
	
	if (strlen($Update) > 0 && strlen($_REQUEST["back_url_settings"]) > 0)
        LocalRedirect($_REQUEST["back_url_settings"]);
    else
        LocalRedirect($APPLICATION->GetCurPage() . "?mid=" . $module_id . "&lang=" . LANG . "&back_url_settings=" . urlencode($_REQUEST["back_url_settings"]) . "&" . $tabControl->ActiveTabParam());

}

$tabControl->Begin();

?>


<form action='<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>' method='post'>

	<?
	foreach($aTabs as $tab)
	{	
		if(!empty($tab['OPTIONS']))
		{
			$tabControl->BeginNextTab();

			if ($tab['DIV'] === 'edit4')
				echo $message->Show();

			foreach ($tab['OPTIONS'] as $option) 
			{
				if ($option[0] === 'active_lazy_load_img_preloader_color')
				{
					$pathPreloader = Option::get($module_id, 'active_lazy_load_img_preloader_path', '');
					$colorPreloader = Option::get($module_id, 'active_lazy_load_img_preloader_color', '');

					if (empty($pathPreloader))
						continue;
				
					?>
					
						<tr>
							
							<td width="50%" class="adm-detail-content-cell-l">
								Путь к изображению <a name="opt_active_lazy_load_img_preloader_path"></a>
							</td>
							<td width="50%" class="adm-detail-content-cell-r" >
								<img src="<?= $pathPreloader; ?>" id="img_preloader"><br>
								<input 
									type="range" 
									min="0" 
									max="360" 
									step="1" 
									value="<?= $colorPreloader; ?>" 
									style="width: 80%;" 
									name="active_lazy_load_img_preloader_color"
									onInput="setStyle(this.value);"	
								>	
							</td>			
						</tr>

						<script>
							function setStyle(val)
							{	
								var imgPreloader = document.getElementById('img_preloader');
								imgPreloader.style.cssText = "filter: hue-rotate(" + val + "deg) !important; -webkit-filter: hue-rotate(" + val + "deg) !important; -moz-filter: hue-rotate(" + val + "deg) !important;"; 
							}		
							setStyle(<?= $colorPreloader; ?>);
						</script>

					<?
				}else{
					__AdmSettingsDrawRow($module_id, $option);
				}
			}

			//__AdmSettingsDrawList($module_id, $tab['OPTIONS']);
		}
	}
	unset($tab, $option);

	$tabControl->Buttons();
	?>

	<input type="submit" name="apply" value="<?= Loc::GetMessage('ALTIT_PAGE_SPEED_INPUT_APPLY'); ?>" class="adm-btn-save" />
	<input type="submit" name="default" value="<?= Loc::GetMessage('ALTIT_PAGE_SPEED_INPUT_DEFAULT'); ?>" />

	<? if (strlen($_REQUEST["back_url_settings"]) > 0): ?>
		
		<input type="hidden" name="back_url_settings" value="<?= htmlspecialcharsbx($_REQUEST["back_url_settings"]) ?>">

	<? endif ?>

	<?
		echo(bitrix_sessid_post());
	?>

</form>

<?
$tabControl->End();


?>

<style>
.adm-info-message {
	display: block!important;
}
</style>

<? if (class_exists('\Bitrix\Main\UI\Extension')):
	\Bitrix\Main\UI\Extension::load("ui.hint");
?>
	<script>
		BX.ready(function() {
			BX.UI.Hint.init(BX('adm-workarea')); 
		});
	</script>

<? endif; ?>