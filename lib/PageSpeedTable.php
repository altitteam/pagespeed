<?php
namespace Altit\PageSpeed;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\TextField;
use Bitrix\Main\Entity\StringField;

class PageSpeedTable extends DataManager
{
    public static function getTableName()
    {
        return 'pagespeed';
    }

    public static function getMap()
    {
        return [
            'ID' => new IntegerField('ID', [
                'primary' => true,
                'autocomplete' => true,
            ]),
            'TIME' => new IntegerField('TIME', [
                'required' => true
            ]),
            'SRC' => new StringField('SRC', [
                'required' => true
            ])
        ];
    }

}