<?php
	namespace Altit\PageSpeed;

	use \Bitrix\Main\Config\Option;
	use \Bitrix\Main\Page\Asset;
	use \Altit\PageSpeed\Helper;
	use \WebPConvert\WebPConvert;

	class Main
	{
		public static $exceptionsPage = [
			'/api.php',
		];

		public static function checkAjaxAndAdmin()
		{
			if(!defined('ADMIN_SECTION') && ADMIN_SECTION !== true && $_SERVER['REQUEST_METHOD'] != 'POST' && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest' && $_SERVER['HTTP_BX_AJAX'] == null && empty($_GET['sessid']))
				return true;
			else
				return false; 
		}

		public static function exceptionsPage ()
		{
			$arExceptions = self::$exceptionsPage;

			if(!empty($arExceptions))
			{
				foreach($arExceptions as $page)
				{
					if($page === $_SERVER['DOCUMENT_URI']) return false;
				}
			}

			return true;
		}

		public function appendScriptsToPage()
		{
			if(self::checkAjaxAndAdmin() && self::exceptionsPage())
			{
				$module_id = pathinfo(dirname(__DIR__))["basename"];
				$css = '';
				$js = '';
				$arOptionLazy = [
					'CLASS' => Option::get($module_id, 'active_lazy_load_img_class', 'lazy_loading'),
					'OBSERVER' => Option::get($module_id, 'active_lazy_load_js', ''),
				];

				if (Option::get($module_id, 'active_module', 'N') === 'Y')
				{
					\CJSCore::Init(['fx']);
					if (Option::get($module_id, 'jquery', 'N') === 'Y')
					{
						\CJSCore::Init(['jquery2']);
					}

					if (Option::get($module_id, 'active_lazy_load_img', 'N') === 'Y' )
					{
						Asset::getInstance()->addJs('/bitrix/js/' . $module_id . '/modul.lazy.min.js');

						$js .= 'var lazyLoadModule = new lazyLoad({class: "' . $arOptionLazy['CLASS'] . '", observer: "' . $arOptionLazy['OBSERVER'] . '"});

							lazyLoadModule.load();

							BX.addCustomEvent("onAjaxSuccess", function () {
								setTimeout(function() {
									lazyLoadModule.load();console.log("onAjaxSuccess");
								}, 0);
							});
							jQuery(document).ajaxSend(function() {
								setTimeout(function() {
									lazyLoadModule.load();console.log("ajaxSend");
								}, 0);
							});';

						$css .= '.' . Option::get($module_id, 'active_lazy_load_img_class', 'lazy_loading') . '{
							opacity: 1;
							transition: opacity 0.3s;
						}
						
						.' . Option::get($module_id, 'active_lazy_load_img_class', 'lazy_loading') . '[data-src] {
							display: block;
							margin: 15px auto 25px;
							filter: hue-rotate(' . Option::get($module_id, 'active_lazy_load_img_preloader_color', '0') . 'deg) !important;
							-webkit-filter: hue-rotate(' . Option::get($module_id, 'active_lazy_load_img_preloader_color', '0') . 'deg) !important; 
							-moz-filter: hue-rotate(' . Option::get($module_id, 'active_lazy_load_img_preloader_color', '0') . 'deg) !important;
						}';

						if (empty(Option::get($module_id, 'active_lazy_load_img_preloader_path', '')))
						{
							$css .= '.' . Option::get($module_id, 'active_lazy_load_img_class', 'lazy_loading') . '{
								opacity: 0;
							}';
						}
					}

					if (Option::get($module_id, 'scripts', 'N') === 'Y')
					{
						$ajaxDir =  '/local/ajax/' . $module_id . '/Ajax.php';
						$js .= 'setTimeout(function() {
							$.get("' . $ajaxDir . '", function(data) {
								$("body").append(data);
							});
						}, 2700);';
					}

					Asset::getInstance()->addString(
						'<script>
							window.addEventListener("load", function(event) {		
								setTimeout(function() {
									' . $js . '
								}, 0);
							});
						</script>
						<style>
						' . $css . '
						</style>',
						true
					);
				}
			}

			return false;
		}
	
		public function OnEndBufferContentHandler (&$content)
		{
			if(self::checkAjaxAndAdmin() && self::exceptionsPage())
			{
				$helper = new Helper;
				$content = $helper->main($content);
			}
		}

		public function convertWebpTest() 
        {
            $arConvertr =  [
                'gd',
                'gmagick', 
                'graphicsmagick', 
                'cwebp', 
                'vips', 
                'imagick', 
                'imagemagick',      
                'wpc',        
            ];

            $testImg = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/images/' . pathinfo(dirname(__DIR__))["basename"] . '/testWebpConvert.png';

            return WebPConvert::convert($testImg, $testImg . '.webp', [ 'converters' => $arConvertr]);
		}
		
		public function fontCache($param) 
        {
            $file = $_SERVER['DOCUMENT_ROOT'] . '/.htaccess';
            $fileOld = $_SERVER['DOCUMENT_ROOT'] . '/.htaccess.olds';
            $fileBackup = $_SERVER['DOCUMENT_ROOT'] . '/.htaccess.backups';
			$text = '	ExpiresActive on
			AddType image/webp .webp
            ExpiresByType image/jpeg "access plus 3 day"
			ExpiresByType image/gif "access plus 3 day"
			ExpiresByType image/webp "access plus 1 year"
        
            # Add correct content-type for fonts
            AddType application/vnd.ms-fontobject .eot 
            AddType application/x-font-ttf .ttf
            AddType application/x-font-opentype .otf
            AddType application/x-font-woff .woff
            AddType application/x-font-woff2 .woff2
            AddType image/svg+xml .svg
        
            ExpiresByType application/vnd.ms-fontobject "access plus 1 year"
            ExpiresByType application/x-font-ttf "access plus 1 year"
            ExpiresByType application/x-font-opentype "access plus 1 year"
            ExpiresByType application/x-font-woff "access plus 1 year"
            ExpiresByType application/x-font-woff2 "access plus 1 year"
            ExpiresByType image/svg+xml "access plus 1 year"';

            if (!file_exists($fileBackup))
            {
                file_put_contents($fileBackup, file_get_contents($file));
            } 
            
            if ($param)
            {
                if (file_exists($file))
                {
                    $htaccessContent = file_get_contents($file);

                    if (!file_exists($fileOld))
                    {
                        file_put_contents($fileOld, $htaccessContent);
                    }                  

                    if (file_get_contents($fileOld) === $htaccessContent)
                    {
                        preg_match("|<IfModule mod_expires.c>(.+?)</IfModule>|isU", $htaccessContent, $pregOut);

                        if (!empty($pregOut[1]))
                        { 
                            $htaccessContent = str_replace($pregOut[0], '<IfModule mod_expires.c>' . $pregOut[1] . "\n". $text . "\n". '</IfModule>', $htaccessContent);
                        } else {
                            $htaccessContent .= "\n";
                            $htaccessContent .= "<IfModule mod_expires.c>\n";
                            $htaccessContent .= $text . "\n";
                            $htaccessContent .= "</IfModule>\n";
                        }
                        file_put_contents($file, $htaccessContent);
                    }
                }
            } else {
                if (file_exists($fileOld))
                {
                    $htaccessContent = file_get_contents($fileOld);
                    file_put_contents($file, $htaccessContent);
                }   
            }     
        }
	}