<?php
	namespace Altit\PageSpeed;

    use \Bitrix\Main\Loader;
    use \Bitrix\Main\Page\Asset;
    use \Altit\PageSpeed\CompressHTML;
    use \Bitrix\Main\Config\Option;
    use \WebPConvert\WebPConvert;
    use Altit\PageSpeed\PageSpeedTable;

    Loader::includeModule('altit.pagespeed'); 

	class Helper 
	{
        protected $arOption;
        protected $module_id;
        protected $arPreg;

        public function __construct()
        {
            $this->module_id = pathinfo(dirname(__DIR__))["basename"];
            
            $this->arOption = [
                'ACTIVE_MODULE' => [
                    'ACTIVE' => Option::get($this->module_id, 'active_module', 'N')
                ],
                'LAZY_LOAD_IMG' => [
                    'ACTIVE' => Option::get($this->module_id, 'active_lazy_load_img', 'N'),
                    'CLASS' => Option::get($this->module_id, 'active_lazy_load_img_class', 'lazy_loading'),
                    'PATH' => Option::get($this->module_id, 'active_lazy_load_img_preloader_path', ''),
                ],
                'IFRAME' => [
                    'ACTIVE' => Option::get($this->module_id, 'iframe_on', 'N'),
                    'CLASS' => Option::get($this->module_id, 'active_lazy_load_img_class', 'lazy_loading')
                ],
                'COMPRESS_HTML' => [
                    'ACTIVE' => Option::get($this->module_id, 'active_compress_html', 'N'),
                    'OPTIONS' => [
                        'css' => (Option::get($this->module_id, 'active_compress_html_css', 'Y') === 'Y') ? true : false,
                        'js' => (Option::get($this->module_id, 'active_compress_html_js', 'N') === 'Y') ? true : false,
                        'comment' => (Option::get($this->module_id, 'active_compress_html_comment', 'N') === 'Y') ? true : false,
                    ]
                ],
                'INLINE_CSS' => [  
                    'ACTIVE' => Option::get($this->module_id, 'inline_css', 'N'),
                    'OPTIONS' => [
                        'exceptions' => [
                            'active' => (Option::get($this->module_id, 'active_inline_css_exceptions', 'N') === 'Y') ? true : false,
                            'value' => Option::get($this->module_id, 'inline_css_exceptions_val', '')
                        ],
                        'font' => [
                            'active' => (Option::get($this->module_id, 'active_inline_css_font', 'N') === 'Y') ? true : false
                        ],
                        'external_css' => [
                            'active' => (Option::get($this->module_id, 'active_inline_css_external', 'N') === 'Y') ? true : false
                        ],
                        'compress_css' => [
                            'active' => (Option::get($this->module_id, 'active_inline_css_compress', 'N') === 'Y') ? true : false
                        ]
                    ]
                ],
                'CONVERT_WEBP' => [
                    'ACTIVE' => Option::get($this->module_id, 'active_convert_webp', 'N'),
                    'OPTIONS' => [
                        'png' => (Option::get($this->module_id, 'active_convert_webp_png', 'Y') === 'Y') ? true : false,
                    ]
                ],
            ];
        }

		public function main (&$content)
		{
            if ($this->arOption['ACTIVE_MODULE']['ACTIVE'] === 'Y')
            {
                if ($this->arOption['CONVERT_WEBP']['ACTIVE'] === 'Y' || $this->arOption['LAZY_LOAD_IMG']['ACTIVE'] === 'Y')
                {
                    $this->arPreg = $this::getPregContent($content);
                }

                if ($this->arOption['CONVERT_WEBP']['ACTIVE'] === 'Y')
                {
                    if ($_GET['d'])
                    {
                        $arMS['CONVERT_WEBP']['TIME'] = microtime(true);
                        $arMS['CONVERT_WEBP']['NAME'] = 'webp';
                    }
                    
                    self::webpToPage($content, $this->arOption['CONVERT_WEBP']['OPTIONS']); 

                    if ($_GET['d'])
                    {
                        $arMS['CONVERT_WEBP']['TIME'] = round((microtime(true) -  $arMS['CONVERT_WEBP']['TIME']) * 1000, 2);
                    }
                }

                if ($this->arOption['LAZY_LOAD_IMG']['ACTIVE'] === 'Y')
                {
                    if ($_GET['d'])
                    {
                        $arMS['LAZY_LOAD_IMG']['TIME'] = microtime(true);
                        $arMS['LAZY_LOAD_IMG']['NAME'] = 'lazyLoadImg';
                    }

                    self::lazyLoadImg($content, $this->arOption['LAZY_LOAD_IMG']['CLASS'], $this->arOption['LAZY_LOAD_IMG']['PATH']); 

                    if ($_GET['d'])
                    {
                        $arMS['LAZY_LOAD_IMG']['TIME'] = round((microtime(true) -  $arMS['LAZY_LOAD_IMG']['TIME']) * 1000, 2);
                    }
                }

                if ($this->arOption['IFRAME']['ACTIVE'] === 'Y')
                {
                    if ($_GET['d'])
                    {
                        $arMS['IFRAME']['TIME'] = microtime(true);
                        $arMS['IFRAME']['NAME'] = 'iframe';
                    }

                    self::iframe($content, $this->arOption['LAZY_LOAD_IMG']['CLASS']);

                    if ($_GET['d'])
                    {
                        $arMS['IFRAME']['TIME'] = round((microtime(true) -  $arMS['IFRAME']['TIME']) * 1000, 2);
                    }
                }
                
                if ($this->arOption['INLINE_CSS']['ACTIVE'] === 'Y' && !self::checkIsAdmin())
                {
                    if ($_GET['d'])
                    {
                        $arMS['INLINE_CSS']['TIME'] = microtime(true);
                        $arMS['INLINE_CSS']['NAME'] = 'inlineCss';
                    }

                    self::inlineCss($content, $this->arOption['INLINE_CSS']['OPTIONS']);

                    if ($_GET['d'])
                    {
                        $arMS['INLINE_CSS']['TIME'] = round((microtime(true) -  $arMS['INLINE_CSS']['TIME']) * 1000, 2);
                    }
                }

                if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') !== false)
                {
                    if ($this->arOption['COMPRESS_HTML']['ACTIVE'] === 'Y')
                    {
                        $content = new CompressHTML($content, $this->arOption['COMPRESS_HTML']['OPTIONS']);
                    }
                }
            }

            if ($_GET['d'])
            {
                $strTable = '<table >
                <tbody>
                  <tr>
                    <th colspan="2"><b>Page Speed</b></th></tr>';
                
                    foreach ($arMS as $ms) {
                        $strTable .= ' <tr>
                            <td width="50%">' . $ms['NAME'] . '</td><td>' . $ms['TIME'] . ' ms</td>
                        </tr>'; 
                    }

                    $strTable .= ' <tr>
                        <td width="50%">Всего</td><td>' . array_sum(array_column($arMS, 'TIME')) . ' ms</td>
                    </tr>'; 
                   
                $strTable .= '</tbody>
                </table>';
                $content = $strTable . $content;
            }
            
            return $content;
        }
        
        private static function checkIsAdmin() 
        {
            global $USER;

            if (!is_object($USER)) 
                $USER = new \CUser();

            return $USER->IsAdmin();
        }

        private function lazyLoadImg(&$content, $class, $path) 
        {
            if (empty($class))
                return $content;

            $pregImgC = $this->arPreg;

           // if($_REQUEST['d'])
            //    pr($this->arPreg);

            if (!empty($pregImgC['img'])) 
            {
                if (!empty($path))
                    $defaultSrc = 'src="/bitrix/images/' . pathinfo(dirname(__DIR__))["basename"] . '/loading.gif" data-src-alt';
                else
                    $defaultSrc = 'data-src-alt';

                foreach($pregImgC['img'] as $imgC)  
                {

                    if (strstr($imgC, ':src') || strstr($imgC, 'src="#"')) 
                        continue;

                    $imgCC = $imgC;

                    if (strstr($imgC, 'class')) 
                    {
                        preg_match_all("/class=[\"](.*)[\"]/U", $imgC, $pregImgClass);

                        if ($pregImgClass[1][0]) 
                        {
                            if ((int)substr_count($imgCC, $pregImgClass[1][0]) > 1)
                                continue;

                            $imgCC = str_replace([$pregImgClass[1][0], 'src'], [$pregImgClass[1][0] . ' ' . $class . '', $defaultSrc], $imgCC);
                        }              
                        elseif($pregImgClass[0][0]) 
                        {
                            $imgCC = str_replace([$pregImgClass[0][0], 'src'], ['class="' . $class . '"', $defaultSrc], $imgCC);
                        }
                    } else {
                        $imgCC = str_replace('src', 'class="' . $class . '" ' . $defaultSrc, $imgCC);
                    }

                    if ($imgC != $imgCC) 
                    {
                        $pattern[] = $imgC;
                        $replacement[] = $imgCC;
                    }
                }        
            }

            if (!empty($pregImgC['background'])) 
            {
                foreach($pregImgC['background']['tag'] as $keyB => $imgB)  
                {
                    if (!empty($path))
                        $defaultBg = '/bitrix/images/' . pathinfo(dirname(__DIR__))["basename"] . '/loading.gif';
                    else
                        $defaultBg = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==';

                    $imgBB = $imgB;

                    if (strstr($imgB, 'class')) 
                    {
                        preg_match_all("/class=[\"](.*)[\"]/U", $imgB, $pregBClass);

                        if ($pregBClass[1][0]) 
                        {
                            if ((int)substr_count($imgBB, $pregBClass[1][0]) > 1)
                                continue;

                            $imgBB = str_replace(
                                [$pregImgC['background']['image'][$keyB], $pregBClass[0][0]], 
                                [$defaultBg, 'class="' . $class . ' ' . $pregBClass[1][0] . '" data-src-alt="' . $pregImgC['background']['image'][$keyB] . '"'], 
                                $imgBB
                            );
                        } elseif($pregBClass[0][0]) {
                            $imgBB = str_replace(
                                [$pregImgC['background']['image'][$keyB], $pregBClass[0][0]], 
                                [$defaultBg, 'class="' . $class . '" data-src-alt="' . $pregImgC['background']['image'][$keyB] . '"'], 
                                $imgBB
                            );
                        }
                    } else {
                        $imgBB = str_replace(
                            [$pregImgC['background']['image'][$keyB], 'style'], 
                            [$defaultBg, 'class="' . $class . '"  data-src-alt="' . $pregImgC['background']['image'][$keyB] . '" style'],
                            $imgBB
                        );
                    }

                    if ($imgB != $imgBB) 
                    {
                        $pattern[] = $imgB;
                        $replacement[] = $imgBB;
                    }
                }  
            }

            if (!empty($pattern) && !empty($replacement)) 
                $content = str_replace($pattern, $replacement, $content);

            return $content;
        }
     
        private static function inlineCss(&$content, $arParam) 
        {
            preg_match_all("/<link[^>]*href=\"([^\"]*)\"[^>]*>/Us", $content, $pregOut);
     
            if (!empty($pregOut[1][0])) 
            {
                $maxFileSize = 1024;
                
                foreach($pregOut[1] as $key => $fileLink) 
                {
                    $fileLink = trim($fileLink);
            
                    if (!strstr($fileLink, '.css')) 
                        continue;
            
                    if (!$arParam['external_css']['active']) 
                    {
                        if (!self::checkExternalLink($fileLink)) 
                            continue;
                    }
                    
                    if (strstr($pregOut[0][$key], 'media="print"') || strstr($pregOut[0][$key], "media='print'")) 
                        continue;
            
            
                    if ($arParam['exceptions']['active'])
                    {
                        $arExceptions = array_map('trim', explode("\n", $arParam['exceptions']['value']));
            
                        if (!empty($arExceptions) && is_array($arExceptions)) 
                        {
                            foreach($arExceptions as $exception) 
                                if (strstr($fileLink, $exception)) 
                                    continue(2);
                            
                        }
                    }
             
                    if (!empty($fileLink) && !empty($pregOut[0][$key])) 
                    {
                        if($cLink = stristr($fileLink, '?', true))
                            $fileLink = $cLink;
                        
                        if (!self::checkExternalLink($fileLink)) 
                        {
                            if (substr($fileLink, 0, 2) === '//') 
                            {
                                $fileLink = 'http:' . $fileLink;
                            }
                            
                            $outCss = self::getInlineCss($fileLink, $arParam['compress_css']['active']);

                        } elseif (file_exists($_SERVER['DOCUMENT_ROOT'] . $fileLink)) {

                            $patchFile = dirname($fileLink) . '/';
                            $fileSize = (filesize($_SERVER['DOCUMENT_ROOT'] . $fileLink) / 1024);
                            
                            if ($fileSize < $maxFileSize) 
                            { 
                                $outCss = self::getInlineCss($fileLink, $arParam['compress_css']['active'], $patchFile); 
                            }
                        }
                        
                        if (strlen($outCss))
                        {
                            $content = str_replace($pregOut[0][$key], '<style type="text/css">' . $outCss . '</style>', $content);
                        }
                    }

                    unset($outCss);
                }
            }

            return $content;
        }

        private static function getInlineCss($fileLink, $minifiCss = false, $patchFile = '') 
        {
            $phpCache = new \CPHPCache();
            $timeCache = 36000;
            $uniq = md5($fileLink);
            $basedir = '/' . SITE_ID . '/' . pathinfo(dirname(__DIR__))["basename"] . '/css/';
            
            if ($phpCache->InitCache($timeCache, $uniq, $basedir)) 
            {
                $getCache = $phpCache->GetVars();
                $content = $getCache['style'];
                
            } elseif($phpCache->StartDataCache()) {
    
                if (!self::checkExternalLink($fileLink)) 
                {
                    $content = self::getExternalContent($fileLink);
                } else {
                    $content = trim(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $fileLink));
                }
                
                $content = self::compressCss(
                    $content, 
                    $minifiCss, 
                    $patchFile
                );
    
                $phpCache->EndDataCache(
                    [
                        'style' => $content
                    ]
                );
            }
            return $content;
        }

        private static function getExternalContent($link) 
        {
            if (extension_loaded('curl')) 
            {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $link);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
                curl_setopt($curl, CURLOPT_HEADER, false);
                $content = curl_exec($curl);
                curl_close($curl);
    
            } else {
                $content = file_get_contents($link);
            }
            return trim($content);
        }

        private static function compressCss($content, $minifiCss, $patchFile = '') 
        {
            $search = [];
            $replace = [];
             
            if ($minifiCss)
            {
                $search[] = '/\/\*.*?\*\//si';
                $search[] = '/(\s)+/s';
                $search[] = '/\n/';
    
                $replace[] = '';
                $replace[] = '\1';
                $replace[] = '';
            }
    
    
            if (!empty($patchFile))
            {
                $search[] = "/url\s?\([\"']?((?!'?\"?data:image)(?!http\:)(?!https\:)[\w\.]+.*)[\"']?\)/Us";
                $replace[] = "url('".$patchFile. "$1')";
            }
    
            if (!empty($search) && !empty($replace)) 
            {
                $content = preg_replace($search, $replace, $content);
            }
                
            return $content;
        }

        private static function checkExternalLink($url) 
        {
            $arUrl = parse_url($url); 

            return empty($arUrl['host']);
        }

        private static function iframe(&$content, $class) 
        {
            if (empty($class))
                return $content;

            preg_match_all('/\<iframe.*\>/U', $content, $pregIframeC);

            if (!empty($pregIframeC[0])) 
            {
                $defaultIframeSrc = 'src="/local/ajax/' . pathinfo(dirname(__DIR__))["basename"] . '/Iframe.php" data-src-alt';

                foreach($pregIframeC[0] as $iframeC)  
                {
                    if (strstr($iframeC, 'www.googletagmanager.com')) 
                        continue;

                    $iframeCC = $iframeC;

                    if (strstr($iframeC, 'class')) 
                    {
                        preg_match_all("/class=[\"](.*)[\"]/U", $iframeC, $pregIframeClass);
                        
                        if ($pregIframeClass[1][0]) 
                            $iframeCC = str_replace([$pregIframeClass[1][0], 'src'], [$pregIframeClass[1][0] . ' ' . $class . '', $defaultIframeSrc], $iframeCC);
                        elseif($pregIframeClass[0][0]) 
                            $iframeCC = str_replace([$pregIframeClass[0][0], 'src'], ['class="' . $class . '"', $defaultIframeSrc], $iframeCC);

                    } else {
                        $iframeCC = str_replace('src', 'class="' . $class . '" ' . $defaultIframeSrc, $iframeCC);
                    }

                    if ($iframeC != $iframeCC) 
                    {
                        $pattern[] = $iframeC;
                        $replacement[] = $iframeCC;
                    }
                }

                if (!empty($pattern) && !empty($replacement)) 
                    $content = str_replace($pattern, $replacement, $content);
            }

            return $content;
        }

        private function webpToPage(&$content, $param) 
        {
            if($_REQUEST['d']){
                $start = microtime(true);
                //header('Content-Type: application/json');
              

                $root = Option::get(pathinfo(dirname(__DIR__))["basename"], 'document_root', '');

                pr(self::getAllSrcTable()['table']['/upload/iblock/d37/d37839f317cfd039853d940c83f18a6d.jpg']);
    
                $arSrc = [];
                //$arTable = self::getAllSrcTable()['src'];
                
                $arConvertr =  [
                    'gd',
                    'gmagick', 
                    'graphicsmagick', 
                    'cwebp',  
                    'imagick', 
                    'imagemagick',             
                ];
                        
                foreach ($arTable as $table)
                {
                    $arSrc[$table['SRC']] = $table;
                }
                
                // if (!empty($this->arPreg['img'])) 
                // {
                //     foreach ($this->arPreg['img'] as &$imgC)  
                //     {
                //         $imgCC = $imgC;

                //         if (!$param['png'])
                //         {
                //             if (!strstr($imgC, '.png')) 
                //             {
                //                 preg_match_all("/src=[\"](.*)[\"]/U", $imgC, $pregImgSrc);
                                
                //                 //$imgCC = $this::convertWebp($pregImgSrc[1][0], $imgCC);

                //                 $checkFileWebp = file_exists($root . $pregImgSrc[1][0] . '.webp');
                    
                                
                //                 if (!empty($pregImgSrc[1][0]) && self::checkExternalLink($pregImgSrc[1][0]) && self::checkFile($pregImgSrc[1][0])) 
                //                 {
                //                     $srcTimeFile = (int)filectime($root . $pregImgSrc[1][0]);
                                    
                //                     if (array_key_exists($pregImgSrc[1][0], $arSrc))
                //                     {   
                //                         $currentSrc = $arSrc[$pregImgSrc[1][0]];
                    
                //                         if ($srcTimeFile !== (int)$currentSrc['TIME'] || !$checkFileWebp)
                //                         {
                //                             $convertCheck = WebPConvert::convert($root . $pregImgSrc[1][0], $root.$pregImgSrc[1][0] . '.webp', [ 'converters' => $arConvertr]);
                                            
                //                             if ($convertCheck['success'])
                //                             {
                //                                 PageSpeedTable::update(
                //                                     (int)$currentSrc['ID'], 
                //                                     [
                //                                         'TIME' => $srcTimeFile
                //                                     ]
                //                                 );
                                                
                //                                 $imgCC = str_replace($pregImgSrc[1][0], $pregImgSrc[1][0] . '.webp', $imgCC);
                //                             }                                   
                //                         } else {
                                            
                //                             $imgCC = str_replace($pregImgSrc[1][0], $pregImgSrc[1][0] . '.webp', $imgCC);   
                                            
                //                         }
                    
                //                     } else {
                //                         $convertCheck = WebPConvert::convert($root . $pregImgSrc[1][0], $root . $pregImgSrc[1][0] . '.webp', [ 'converters' => $arConvertr]);
                                        
                //                         if ($convertCheck['success'])
                //                         {
                //                             PageSpeedTable::add([
                //                                 'TIME' => $srcTimeFile,
                //                                 'SRC' => $pregImgSrc[1][0],
                //                             ]);
                    
                //                             $imgCC = str_replace($pregImgSrc[1][0], $pregImgSrc[1][0] . '.webp', $imgCC);
                //                         } 
                //                     }
                                    
                //                 }
                                
                                
                //             }
                //         } else {
                //             preg_match_all("/src=[\"](.*)[\"]/U", $imgC, $pregImgSrc);

                //             //$imgCC = $this::convertWebp($pregImgSrc[1][0], $imgCC);
                //         }
                        
                //         if ($imgC != $imgCC) 
                //         {
                //             $pattern[] = $imgC; 
                //             $replacement[] = $imgCC;
                //             $imgC = $imgCC;
                //         } 
                //     }     
                // }
                
                // if (!empty($this->arPreg['background']['image'])) 
                // {
                //     foreach ($this->arPreg['background']['image'] as &$imgB)  
                //     {
                //         $imgBB = $imgB;
                        
                //         if (!$param['png'])
                //         {
                //             if (!strstr($imgC, '.png')) 
                //             {                     
                //                 //$imgBB = $this::convertWebp($imgBB, $imgBB);
                //             }
                //         } else {
                //             //$imgBB = $this::convertWebp($imgBB, $imgBB);
                //         }

                //         if ($imgB != $imgBB) 
                //         {
                //             $pattern[] = $imgB;
                //             $replacement[] = $imgBB;
                //             $imgB = $imgBB;
                //         }
                //     }     
                // }

                $start = microtime(true);
                if (!empty($pattern) && !empty($replacement)) 
                    $content = str_replace($pattern, $replacement, $content);


                $time = ((microtime(true) - $start) * 1000) + $time; echo $time . '<br>';
                //$time = (microtime(true) - $start) * 1000; echo $time;
                return $content;
            }
        }

        /*private function convertWebp($link, $imgCC) 
        {
            $root = Option::get(pathinfo(dirname(__DIR__))["basename"], 'document_root', '');

            if (empty($root))
                return $imgCC;

            $arSrc = [];
            $arTable = self::getAllSrcTable()['table'];
            $checkFileWebp = file_exists($root . $link . '.webp');
            $arConvertr =  [
                'gd',
                'gmagick', 
                'graphicsmagick', 
                'cwebp',  
                'imagick', 
                'imagemagick',             
            ];
                    
            foreach ($arTable as $table)
            {
                $arSrc[$table['SRC']] = $table;
            }

            if($_REQUEST['d'])
            {
                //pr(self::getAllSrcTable());
            }

            //$start = microtime(true);
            if (!empty($link) && self::checkExternalLink($link) && self::checkFile($link)) 
            {
                $srcTimeFile = (int)filectime($root . $link);
                
                if (array_key_exists($link, $arSrc))
                {   
                    $currentSrc = $arSrc[$link];

                    if ($srcTimeFile !== (int)$currentSrc['TIME'] || !$checkFileWebp)
                    {
                        $convertCheck = WebPConvert::convert($root . $link, $root.$link . '.webp', [ 'converters' => $arConvertr]);
                        
                        if ($convertCheck['success'])
                        {
                            PageSpeedTable::update(
                                (int)$currentSrc['ID'], 
                                [
                                    'TIME' => $srcTimeFile
                                ]
                            );
                            
                            $imgCC = str_replace($link, $link . '.webp', $imgCC);
                        }                                   
                    } else {
                        $imgCC = str_replace($link, $link . '.webp', $imgCC);   
                    }

                } else {
                    $convertCheck = WebPConvert::convert($root . $link, $root . $link . '.webp', [ 'converters' => $arConvertr]);
                    
                    if ($convertCheck['success'])
                    {
                        PageSpeedTable::add([
                            'TIME' => $srcTimeFile,
                            'SRC' => $link,
                        ]);

                        $imgCC = str_replace($link, $link . '.webp', $imgCC);
                    } 
                }
            }
            //$time = (microtime(true) - $start) * 1000; echo $time;
            return $imgCC;
        }*/

        private function getPregContent($content) 
        {
            $phpCache = new \CPHPCache();
            $timeCache = 36000;
            $dir = array_pop(explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/')));
            $uniq = md5(SITE_ID . LANGUAGE_ID . SITE_TEMPLATE_ID . $dir);
            $basedir = '/' . SITE_ID . '/' . pathinfo(dirname(__DIR__))["basename"] . '/preg/';
            $result = [];
            
            if ($phpCache->InitCache($timeCache, $uniq, $basedir)) 
            {
                $result = $phpCache->GetVars();          
            } elseif ($phpCache->StartDataCache()) {
    
                preg_match_all ('/\<img.*\>/Us', $content, $pregImgC);
                preg_match_all('~<[a-z]{1,}\s.+\bbackground(-image)?\s*:(.*?)\(\s*(\'|")?(?<image>.*?)\3?\s*\).+>~i', $content, $pregImgB);

                $result = [
                    'img' => $pregImgC[0],
                    'background' => [
                        'tag' => $pregImgB[0],
                        'image' => $pregImgB['image']
                    ]
                ];
    
                $phpCache->EndDataCache($result);
            }

            return $result;
        }

        private function getWebpSrc() 
        {
            $root = Option::get(pathinfo(dirname(__DIR__))["basename"], 'document_root', '');

            if (empty($root))
                return false;

            $arSrc = [];
            $result = [];
            $complete = true;
            $arConvertr =  [
                'gd',
                'gmagick', 
                'graphicsmagick', 
                'cwebp',  
                'imagick', 
                'imagemagick',             
            ];
            $startTime = (microtime(true) * 1000) + 3000;

            $getSrc = PageSpeedTable::getList([
                'order' => [
                    'ID' => 'ASC'
                ],
            ]);

            while ($src = $getSrc->Fetch())
            {
                $arSrc[$src['SRC']] = $src;
            }

            if (!empty($this->arPreg['img'])) 
            {
                foreach ($this->arPreg['img'] as $imgC)  
                {
                    if ((microtime(true) * 1000) >= $startTime)
                    {
                        $complete = false;
                        break;
                    }

                    preg_match_all("/src=[\"](.*)[\"]/U", $imgC, $pregImgSrc);

                    $checkFileWebp = file_exists($root . $pregImgSrc[1][0] . '.webp');
                    
                    if (!empty($pregImgSrc[1][0]) && self::checkExternalLink($pregImgSrc[1][0]) && self::checkFile($pregImgSrc[1][0])) 
                    {
                        $srcTimeFile = (int)filectime($root . $pregImgSrc[1][0]);
                        
                        if (array_key_exists($pregImgSrc[1][0], $arSrc))
                        {   
                            $currentSrc = $arSrc[$pregImgSrc[1][0]];
        
                            if ($srcTimeFile !== (int)$currentSrc['TIME'] || !$checkFileWebp)
                            {
                                $convertCheck = WebPConvert::convert($root . $pregImgSrc[1][0], $root.$pregImgSrc[1][0] . '.webp', [ 'converters' => $arConvertr]);
                                
                                if ($convertCheck['success'])
                                {
                                    PageSpeedTable::update(
                                        (int)$currentSrc['ID'], 
                                        [
                                            'TIME' => $srcTimeFile
                                        ]
                                    );
                                    
                                    $arSrc[$pregImgSrc[1][0]]['TIME'] = $srcTimeFile;
                                }                                   
                            }
                        } else {
                            $convertCheck = WebPConvert::convert($root . $pregImgSrc[1][0], $root . $pregImgSrc[1][0] . '.webp', [ 'converters' => $arConvertr]);
                            
                            if ($convertCheck['success'])
                            {
                                PageSpeedTable::add([
                                    'TIME' => $srcTimeFile,
                                    'SRC' => $pregImgSrc[1][0],
                                ]);
        
                                $arSrc[$pregImgSrc[1][0]] = [
                                    'SRC' => $pregImgSrc[1][0],
                                    'TIME' => $srcTimeFile
                                ];
                            } 
                        }
                        
                    }
                }     
            }

            if (!empty($this->arPreg['background']['image'])) 
            {
                foreach ($this->arPreg['background']['image'] as $imgBB)  
                {
                    if ((microtime(true) * 1000) >= $startTime)
                    {
                        $complete = false;
                        break;
                    }

                    if (!empty($imgBB) && self::checkExternalLink($imgBB) && self::checkFile($imgBB)) 
                    {
                        $srcTimeFile = (int)filectime($root . $imgBB);
                        $checkFileWebp = file_exists($root . $imgBB . '.webp');
                        
                        if (array_key_exists($imgBB, $arSrc))
                        {   
                            $currentSrc = $arSrc[$imgBB];

                            if ($srcTimeFile !== (int)$currentSrc['TIME'] || !$checkFileWebp)
                            {
                                $convertCheck = WebPConvert::convert($root . $imgBB, $root . $imgBB . '.webp', [ 'converters' => $arConvertr]);
                                
                                if ($convertCheck['success'])
                                {
                                    PageSpeedTable::update(
                                        (int)$currentSrc['ID'], 
                                        [
                                            'TIME' => $srcTimeFile
                                        ]
                                    );
                                    
                                    $arSrc[$imgBB]['TIME'] = $srcTimeFile;
                                }                                   
                            }
                        } else {
                            $convertCheck = WebPConvert::convert($root . $imgBB, $root . $imgBB . '.webp', [ 'converters' => $arConvertr]);
                            
                            if ($convertCheck['success'])
                            {
                                PageSpeedTable::add([
                                    'TIME' => $srcTimeFile,
                                    'SRC' => $imgBB,
                                ]);
        
                                $arSrc[$imgBB] = [
                                    'SRC' => $imgBB,
                                    'TIME' => $srcTimeFile
                                ];
                            } 
                        }
                    }

                }     
            }

            return $result = [
                'table' => $arSrc,
                'complete' => $complete
            ];
        }

        private function getAllSrcTable() 
        {
            $phpCache = new \CPHPCache();
            $timeCache = 36000;
            $dir = array_pop(explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/')));
            $uniq = md5(SITE_ID . LANGUAGE_ID . SITE_TEMPLATE_ID . $dir);
            $basedir = '/' . SITE_ID . '/' . pathinfo(dirname(__DIR__))["basename"] . '/table/';
            $result = [];

            if ($phpCache->InitCache($timeCache, $uniq, $basedir)) 
            {
                $result = $phpCache->GetVars();          
            } elseif ($phpCache->StartDataCache()) {

                $arSrc = self::getWebpSrc();

                $result = $arSrc;
                var_dump($arSrc['complete']);
                if ($arSrc['complete'])
                    $phpCache->EndDataCache($result);
                else {
                    $phpCache->AbortDataCache();
                    $phpCache->Clean($uniq, $basedir);
                }       
            }

            return $result;
        }

        private static function checkFile($url) 
        {
            $root = Option::get(pathinfo(dirname(__DIR__))["basename"], 'document_root', '');
            $image = $root . $url;
            $imageType = [
                IMAGETYPE_JPEG,
                IMAGETYPE_PNG
            ];

            if (empty($root))
                return false;

            if (file_exists($image) && is_file($image) && in_array(exif_imagetype($image), $imageType))
            {
                return true; 
            }

            return false;
        }
    }
