<?php
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	$arComponentDescription = array(
    	'NAME' => Loc::getMessage('ALT_LAZY_COMPONENT_NAME'),
    	'DESCRIPTION' => Loc::getMessage('ALT_LAZY_COMPONENT_NAME_DESCRIPTION'),
    	'ICON' => "/images/sections_top_count.gif",
    	'CACHE_PATH' => "Y",
    	'SORT' => 20,
    	'PATH' => array(
    		'ID' => Loc::getMessage('ALT_COMPONENT')
    	),
    );