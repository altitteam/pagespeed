<?php
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
	/** @var array $arParams */
	/** @var array $arResult */
	/** @global CMain $APPLICATION */
	/** @global CUser $USER */
	/** @global CDatabase $DB */
	/** @var CBitrixComponentTemplate $this */
	/** @var string $templateName */
	/** @var string $templateFile */
	/** @var string $templateData */
	/** @var string $templateFolder */
	/** @var string $componentPath */
	/** @var CBitrixComponent $component */

	use \Bitrix\Main\Config\Option;

	$signer = new \Bitrix\Main\Security\Sign\Signer;

	$templateData['AJAX_ID'] = CAjax::getComponentID($this->__name, '.default', '');
	$signedComponent = $signer->sign($arParams['component']);
	$signedTemplate = $signer->sign($arParams['template']);
	$signedParams = $signer->sign(base64_encode(serialize($arParams['parameters'])));
?>

<div id="comp_lazy_<?= $templateData['AJAX_ID']; ?>" ></div>

<script>

	document.addEventListener("DOMContentLoaded", function(){
		if (window.lazyLoadModule) 
		{
			lazyLoadModule.loadComponent({
				siteId: '<?= CUtil::JSEscape($component->getSiteId()); ?>',
				component: '<?= CUtil::JSEscape($signedComponent); ?>',
				template: '<?= CUtil::JSEscape($signedTemplate); ?>',
				parameters: '<?= CUtil::JSEscape($signedParams); ?>',
				ajaxId: '<?= CUtil::JSEscape($templateData['AJAX_ID']); ?>',
				componentPath: '<?= CUtil::JSEscape($componentPath); ?>'
			});
		} else {

			lazyComponent({
				siteId: '<?= CUtil::JSEscape($component->getSiteId()); ?>',
				component: '<?= CUtil::JSEscape($signedComponent); ?>',
				template: '<?= CUtil::JSEscape($signedTemplate); ?>',
				parameters: '<?= CUtil::JSEscape($signedParams); ?>',
				ajaxId: '<?= CUtil::JSEscape($templateData['AJAX_ID']); ?>',
				componentPath: '<?= CUtil::JSEscape($componentPath); ?>'
			});

			function lazyComponent (data)
			{
				BX.ajax({
					url: data.componentPath + '/ajax.php' + (document.location.href.indexOf('clear_cache=Y') !== -1 ? '?clear_cache=Y' : ''),
					method: 'POST',
					//dataType: 'json',
					timeout: 60,
					async: true,
					data: {
						siteId: data.siteId,
						component: data.component,
						template: data.template,
						parameters: data.parameters,
						rq_url: "<?=POST_FORM_ACTION_URI?>"
					},
					onsuccess: function(result){
						if (result == '')
							return;
							
						var ob = BX.processHTML(result);
						BX('comp_lazy_' + data.ajaxId).innerHTML = ob.HTML;
						BX.ajax.processScripts(ob.SCRIPT);
					}
				});
			}
		}
	});

</script>