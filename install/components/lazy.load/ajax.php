<?
/** @global \CMain $APPLICATION */
define('STOP_STATISTICS', true);
define('NOT_CHECK_PERMISSIONS', true);

$siteId = isset($_REQUEST['siteId']) && is_string($_REQUEST['siteId']) ? $_REQUEST['siteId'] : '';
$siteId = substr(preg_replace('/[^a-z0-9_]/i', '', $siteId), 0, 2);
if (!empty($siteId) && is_string($siteId))
{
	define('SITE_ID', $siteId);
}

if (!empty($_REQUEST['rq_url']))
	define('POST_FORM_ACTION_URI', $_REQUEST['rq_url']);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
	return;

$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
    $component = $signer->unsign($request->get('component'));
	$template = $signer->unsign($request->get('template'));
    $paramString = $signer->unsign($request->get('parameters'));
	$parameters = array_map('htmlspecialcharsBack', unserialize(base64_decode($paramString)));
}
catch (BadSignatureException $e)
{
	die();
}

$APPLICATION->IncludeComponent(
	$component,
	$template,
    $parameters,
    false
);