<?php
	if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

	use Bitrix\Main\Localization\Loc;
	Loc::loadMessages(__FILE__);

	$arComponentParameters = array(
		'PARAMETERS' => array(
			"component" => array(
				"PARENT" => "BASE",
				"NAME" => "component",
				"TYPE" => "STRING",
				"DEFAULT" => "",
			),
			"template" => array(
				"PARENT" => "BASE",
				"NAME" => "template",
				"TYPE" => "STRING",
				"DEFAULT" => "",
			),
			"parameters" => array(
				"PARENT" => "BASE",
				"NAME" => "parameters",
				"TYPE" => "STRING",
				"DEFAULT" => "",
			),
		),
	);

