<?php
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

    use \Bitrix\Main\Config\Option;
    use Bitrix\Main\Loader;
    
    Loader::includeModule('altit.pagespeed'); 

?>



    <?= Option::get('altit.pagespeed', 'scripts_value', ''); ?>

