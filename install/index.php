<?Php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Altit\PageSpeed\PageSpeedTable;

Loc::loadMessages(__FILE__);

class altit_pagespeed extends CModule
{

	public function __construct()
	{
		if(file_exists(__DIR__."/version.php"))
		{

			$arModuleVersion = array();

			include(__DIR__."/version.php");

			$this->MODULE_ID 		   = str_replace("_", ".", get_class($this));
			
			if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
				$this->MODULE_VERSION 	   = $arModuleVersion["VERSION"];
				$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
			} else {
				$this->MODULE_VERSION = 0;
				$this->MODULE_VERSION_DATE = 0;
			}

			$this->MODULE_NAME 		   = Loc::getMessage("ALTIT_PAGE_SPEED_NAME");
			$this->MODULE_DESCRIPTION  = Loc::getMessage("ALTIT_PAGE_SPEED_DESCRIPTION");
			$this->PARTNER_NAME 	   = Loc::getMessage("ALTIT_PAGE_SPEED_PARTNER_NAME");
			$this->PARTNER_URI  	   = Loc::getMessage("ALTIT_PAGE_SPEED_PARTNER_URI");
		}

		return false;
	}

	public function DoInstall()
	{
		global $APPLICATION;

		if(CheckVersion(ModuleManager::getVersion("main"), "14.00.00"))
		{
			ModuleManager::registerModule($this->MODULE_ID);

			$this->InstallFiles();
			$this->InstallDB();

			$this->InstallEvents();
		}else{
			$APPLICATION->ThrowException(
				Loc::getMessage("ALTIT_PAGE_SPEED_INSTALL_ERROR_VERSION")
			);
		}

		$APPLICATION->IncludeAdminFile(
			Loc::getMessage("ALTIT_PAGE_SPEED_INSTALL_TITLE")." \"".Loc::getMessage("ALTIT_PAGE_SPEED_NAME")."\"",
			__DIR__."/step.php"
		);

		return false;
	}

	public function InstallFiles()
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/" . $this->MODULE_ID . "/install/ajax", $_SERVER["DOCUMENT_ROOT"]."/local/ajax/" . $this->MODULE_ID, true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/" . $this->MODULE_ID . "/install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js/" . $this->MODULE_ID, true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/" . $this->MODULE_ID . "/install/images", $_SERVER["DOCUMENT_ROOT"]."/bitrix/images/" . $this->MODULE_ID, true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/" . $this->MODULE_ID . "/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/" . $this->MODULE_ID, true, true);

		return true;
	}

	public function InstallDB()
	{
		if (Loader::includeModule($this->MODULE_ID))
		{
			PageSpeedTable::getEntity()->createDbTable();
		}
		return true;
	}

	public function InstallEvents()
	{

		$eventManager = EventManager::getInstance();

		$eventManager->registerEventHandler(
			"main",
			"OnBeforeEndBufferContent",
			$this->MODULE_ID,
			"Altit\PageSpeed\Main",
			"appendScriptsToPage"
		);

		$eventManager->registerEventHandler(
			"main",
			"OnEndBufferContent",
			$this->MODULE_ID,
			"Altit\PageSpeed\Main",
			"OnEndBufferContentHandler"
		);

		return false;
	}

	public function DoUninstall()
	{

		global $APPLICATION;

		$this->UnInstallFiles();
		$this->UnInstallDB();
		$this->UnInstallEvents();

		ModuleManager::unRegisterModule($this->MODULE_ID);

		$APPLICATION->IncludeAdminFile(
			Loc::getMessage("ALTIT_PAGE_SPEED_UNINSTALL_TITLE")." \"".Loc::getMessage("ALTIT_PAGE_SPEED_NAME")."\"",
			__DIR__."/unstep.php"
		);

		return false;
	}

	public function UnInstallFiles()
	{
		DeleteDirFilesEx("/local/ajax/" . $this->MODULE_ID);
		DeleteDirFilesEx("/bitrix/js/" . $this->MODULE_ID);
		DeleteDirFilesEx("/bitrix/images/" . $this->MODULE_ID);
		DeleteDirFilesEx("/bitrix/components/" . $this->MODULE_ID);

		return true;
	}

	public function UnInstallDB()
	{
		if (Loader::includeModule($this->MODULE_ID))
		{
			$connection = Application::getInstance()->getConnection();
			$connection->dropTable(PageSpeedTable::getTableName());
		}

		Option::delete($this->MODULE_ID);

		return true;
	}

	public function UnInstallEvents()
	{
		$eventManager = EventManager::getInstance();

		$eventManager->unRegisterEventHandler(
			"main",
			"OnBeforeEndBufferContent",
			$this->MODULE_ID,
			"Altit\PageSpeed\Main",
			"appendScriptsToPage"
		);

		$eventManager->unRegisterEventHandler(
			"main",
			"OnEndBufferContent",
			$this->MODULE_ID,
			"Altit\PageSpeed\Main",
			"OnEndBufferContentHandler"
		);

		return false;
	}
}