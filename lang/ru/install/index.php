<?Php

$MESS["ALTIT_PAGE_SPEED_NAME"] 		   			= "PageSpeed";
$MESS["ALTIT_PAGE_SPEED_DESCRIPTION"]  			= "Ускоряет загрузку сайта для.";
$MESS["ALTIT_PAGE_SPEED_PARTNER_NAME"] 			= "Alt-IT Solutions";
$MESS["ALTIT_PAGE_SPEED_PARTNER_URI"]  			= "";
$MESS["ALTIT_PAGE_SPEED_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["ALTIT_PAGE_SPEED_INSTALL_TITLE"]   		= "Установка модуля";
$MESS["ALTIT_PAGE_SPEED_UNINSTALL_TITLE"] 		= "Деинсталляция модуля";